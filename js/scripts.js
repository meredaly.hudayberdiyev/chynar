$('.__js-slider').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
})
$('.__js-slider-3').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1025,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
})

$('.reg-inputs').autotab({ format: 'number' })

var swiper = new Swiper('.product-slider__vertical', {
  loop: false,
  slidesPerView: null,
  freeMode: true,
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
  direction: 'vertical',
  noSwiping: false,
  autoplay: false,
  keyboard: false,
})
var swiper2 = new Swiper('.product-slider__horizontal', {
  loop: false,
  slidesPerView: 1,
  spaceBetween: 10,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  thumbs: {
    swiper: swiper,
  },
})

$('#p-hidden').click(function () {
  $('.p-password').attr('type', 'text')
  $(this)[0].style.display = 'none'
  $('#p-visible')[0].style.display = 'block'
})

$('#p-visible').click(function () {
  $('.p-password').attr('type', 'password')
  $(this)[0].style.display = 'none'
  $('#p-hidden')[0].style.display = 'block'
})

$('.js-search').click(function () {
  $('.header__search').toggleClass('active')
})

$('.menu__burger').click(function (event) {
  $('.menu__burger, .menu__body').toggleClass('active')
  $('body').toggleClass('_lock')
})
$('.menu__second-arrow').click(function (event) {
  $(this).toggleClass('active')
  $(this).parent().children('.menu__second').toggleClass('active')
})

$('.p-category__title').click(function (event) {
  $(this).toggleClass('active')
  if ($(this).hasClass('active')) {
    $('.p-category__row').slideDown()
  } else {
    $('.p-category__row').slideUp()
  }
})

$('.-js-popup').click(function (event) {
  $('.payment-popup').toggleClass('active')
})

$('.product__go-to-trash').click(function (event) {
  $(this).addClass('close')
  $(this).parent().find('.product__count').addClass('active')
})
$('.product__like').click(function (event) {
  $(this).toggleClass('added')
})

$('.profile__controllers-menu ').click(function (event) {
  $(this).toggleClass('active')
  if ($(this).hasClass('active')) {
    $('.profile__controllers-box').slideDown()
  } else {
    $('.profile__controllers-box').slideUp()
  }
})
